export default $axios => ({
    get (params) {
        return $axios.$get('/users' + { params })
    }
})
