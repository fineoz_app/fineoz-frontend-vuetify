export default $axios => ({
    cobaGet() {
        return $axios.$get('/customers');
    },

    cobaPostAiCheck(id, params)
    {
        return $axios.$post(`/customers/${id}/aicheck`, params)
    },

    cobaPostAiScore(id, params)
    {
        return $axios.$post(`/customers/${id}/aiscore`, params)
    },


    cobaGetAiCheck(id)
    {
        return $axios.$get(`/customers/${id}/aicheck`)
    },
    cobaGetAiScore(id)
    {
        return $axios.$get(`/customers/${id}/aiscore`)
    },
    cobaGetAiCheck_Score(id)
    {
        return $axios.$get(`/customers/${id}/aicheck/score`)
    },
    cobaGetAiScore_Score(id)
    {
        return $axios.$get(`/customers/${id}/aiscore/score`)
    },


    get (params) {
        return $axios.$get('/customers', { params })
    },

    getById (id) {
        return $axios.$get('/customers/' + id)
    },

    create (params) {
        return $axios.$post('/customers', params)
    },

    update (params) {
        return $axios.$put('/customers/' + params.id, params)
    },

    destroy (id) {
        return $axios.$put('/customers/' + id)
    },

    aicheck: {
        get (id) {
            return $axios.$get(`/customers/${id}/aicheck`)
        },
        post ({ id, ...params }) {
            return $axios.$post(`/customers/${id}/aicheck`, params)
        },
        put ({ id, ...params }) {
            return $axios.$put(`/customers/${id}/aicheck`, params)
        },
        getScore (id) {
            return $axios.$get(`/customers/${id}/aicheck/score`)
        }
    },

    aiscore: {
        get (id) {
            return $axios.$get(`/customers/${id}/aiscore`)
        },
        post ({ id, ...params }) {
            return $axios.$post(`/customers/${id}/aiscore`, params)
        },
        put ({ id, ...params }) {
            return $axios.$put(`/customers/${id}/aiscore`, params)
        },
        getScore (id) {
            return $axios.$get(`/customers/${id}/aiscore/score`)
        }
    }
})
