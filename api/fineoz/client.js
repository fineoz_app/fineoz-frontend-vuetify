export default $axios => ({
    get (params) {
        return $axios.$get('/clients' + { params })
    }
})
