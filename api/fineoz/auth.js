export default $axios => ({
    login (params) {
        let body = {
            usernameOrEmail: params.usernameOrEmail,
            password: params.password
        }
        return $axios.$post('/auth/login', body)
    },

    logout () {},

    user () {
        return $axios.$get('/auth')
    }
})
