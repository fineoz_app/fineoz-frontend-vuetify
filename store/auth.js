export const state = () => ({
    token: '',
    id: -1,
    email: '',
    name: ''
})

export const mutations = {
    SET_TOKEN (state, payload) {
        state.token = payload
    },
    SET_USER (state, payload) {
        state.id = payload.id
        state.email = payload.email
        state.name = payload.name
    }
}

export const actions = {
    async login ({ commit }, params) {
        let res = await this.$api.auth.login(params)
        if (res) {
            let token = res.data.token
            localStorage._token_local = token
            this.$axios.setToken(token)
            this.dispatch('auth/fetchUser')
            this.$router.push('/')
        }
        return res
    },
    
    logout ({ commit }) {
        commit('SET_TOKEN', '')
        localStorage._token_local = null
        this.$router.push('/login')
        return true
    },
    
    loadToken ({ commit }) {
        let token = localStorage._token_local
        if (token) {
            commit('SET_TOKEN', token)
            this.$axios.setToken(token)
            this.dispatch('auth/fetchUser')
        } else {
            this.$router.push('/login')
        }
        return token
    },

    async fetchUser ({ commit }) {
        let res = await this.$api.auth.user()
        if (res) {
            commit('SET_USER', res.data)
        }
    }
}

export const getters = {
    loggedIn (state) {
        return !!state.token
    }
}
