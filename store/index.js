export const state = () => ({
    drawer: true
});

export const mutations = {
    toggleDrawer(state) {
        state.drawer = !state.drawer
    },
    drawer(state, val) {
        state.drawer = val
    },
    SET_SNACK (state, snack) {
        state.snack = snack
    }
};

export const actions = {
    snackbar(ctx, snack) {
        const _snack = {
            open: true,
            message: '',
            color: undefined,
            timeout: 1500,
            icon: '',
            ...snack
        };
        ctx.commit('SET_SNACK', _snack);
    }
};
