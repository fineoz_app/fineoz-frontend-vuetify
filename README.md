# Fineoz Frontend (Demo Propose)

**Cara Menjalankan Aplikasi**

1. _Clone_ repo ini
2. Buka **Terminal**, masuk ke direktori "fineoz-frontend-vuetify". Caranya ketikkan perintah ```cd fineoz-frontend-vuetify```
3. Ketik perintah ```yarn install```, tunggu instalasi selesai
4. Ketik perintah ```yarn dev``` untuk menjalankan aplikasi
5. Kemudian buka web browser kesayangan Anda, ketikkan ```http://localhost:3000```
6. Masukkan _username_ ```fineoz``` dan _password_ ```fineoz123456```
