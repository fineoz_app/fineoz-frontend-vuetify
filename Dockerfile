FROM node:dubnium-alpine

ENV APP_ROOT /app

RUN mkdir -p ${APP_ROOT}
WORKDIR ${APP_ROOT}
ADD . ${APP_ROOT}

ENV HOST 0.0.0.0
ENV NODE_ENV=production

RUN apk add --no-cache git
RUN npm i -g yarn
RUN yarn install --production
RUN yarn build

EXPOSE 3000
# CMD ["yarn", "start"]
