export default async ({ store, route }) => {
    if (route.name === 'login') {
        return
    }

    let loggedIn = store.getters['auth/loggedIn']

    if (!loggedIn) {
        store.dispatch('auth/loadToken')
    }
}
