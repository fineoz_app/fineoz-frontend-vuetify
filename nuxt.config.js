const pkg = require('./package')
require('dotenv').config()

const VuetifyLoaderPlugin = require('vuetify-loader/lib/plugin')

module.exports = {
    // !!! IMPORTANT: do not run in universal or ssr mode !!!
    mode: 'spa',

    /*
    ** Router config
    */
    router: {
        middleware: [
            'auth'
        ]
    },

    /*
    ** Headers of the page
    */
    head: {
        title: 'Fineoz Dashboard App',
        meta: [
            {charset: 'utf-8'},
            {name: 'viewport', content: 'width=device-width, initial-scale=1'}
        ],
        link: [
            {rel: 'icon', type: 'image/x-icon', href: 'http://fineoz.com/assets/images/fineoz-logo-icon-logo-02-128x99.png'},
            {rel: "stylesheet", href: 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css'},
            {rel: "stylesheet", href: 'https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css'},
            {rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons'},
            {rel: 'stylesheet', href: 'https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css'},
        ],
        script: [
            {src: 'https://cdnjs.cloudflare.com/ajax/libs/echarts/4.0.4/echarts-en.min.js'},
            {src: 'https://code.jquery.com/jquery-3.3.1.js'},
            {src: 'https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js'},
            {src: 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js'}
        ]
    },

    /*
    ** Customize the progress-bar color
    */
    loading: {color: '#3adced'},

    /*
    ** Global CSS
    */
    css: [
        '~/assets/style/theme.styl',
        '~/assets/style/app.styl',
        'font-awesome/css/font-awesome.css',
        'roboto-fontface/css/roboto/roboto-fontface.css',
        '~/assets/scss/main.scss'
    ],

    /*
    ** Plugins to load before mounting the App
    */
    plugins: [
        '@/plugins/axios',
        '@/plugins/api',
        '@/plugins/vuetify',
        '@/plugins/vee-validate',
        '@/plugins/vue-chartist'
    ],

    /*
    ** Nuxt.js modules
    */
    modules: [
        '@nuxtjs/dotenv',
        '@nuxtjs/axios'
    ],

    /*
    ** Build configuration
    */
    build: {
        transpile: ['vuetify/lib'],
        plugins: [new VuetifyLoaderPlugin()],
        loaders: {
            stylus: {
                import: ["~assets/style/variables.styl"]
            }
        },

        /*
        ** You can extend webpack config here
        */
        extend(config, ctx) {

        }
    },
    bootstrapVue: {
        bootstrapCSS: true, // or `css`
        bootstrapVueCSS: true // or `bvCSS`
    }
}
