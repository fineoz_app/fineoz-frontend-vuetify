const qs = require('query-string')

export default function ({ $axios, isDev }) {
    $axios.onRequest(config => {
        console.log('__config', config)
    })

    $axios.onResponse(res => {
        if (isDev) console.warn('[AXIOS] res', res)
        return res
    })

    $axios.onError(err => {
        if (isDev) console.error('[AXIOS] err', err)
        return { err }
    })

    $axios.setHeader('Accept', 'application/json; charset=utf-8')
    $axios.setHeader('X-Requested-With', 'XMLHttpRequest')
    $axios.defaults.paramsSerializer = params => {
        return qs.stringify(params)
    }
}
