import Vue from 'vue'
import VueChartist from 'vue-chartist'
import ChartistPluginLegend from 'chartist-plugin-legend'

Vue.use(VueChartist)
