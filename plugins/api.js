import createAuthApi from '~/api/fineoz/auth'
import createCustomerApi from '~/api/fineoz/customer'

export default ({ $axios }, inject) => {
  const api = {
    auth: createAuthApi($axios),
    customer: createCustomerApi($axios)
  }

  inject('api', api)
}
